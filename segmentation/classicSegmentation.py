import cv2
import numpy as np

from segmentation.preprocessing import background, image_histogram_equalization
from skimage import morphology

# img: [y,x],[frame,y,x],[frame,y,x,channel]
def segmentImg(img, gridX=8, gridY=8, threshold=0.85, minSizeObjects=128*3, minSizeHoles=128*6):
    """ segments cells using non machine-learning methods

    Parameters
    ----------
    img : np.array
        Img to be segmented. It may have the [y,x], [frame,y,x], [frame,y,x,channel]
    gridX : np.uint
        Sets number of vertical sections used for background interpolation
    gridY : np.uint
        sets number of horizontal sections used for background interpolation
    threshold : np.float
        value with which the img with normalized background is to be thresholded with

    Returns
    -------
    array_like
        Array with same shape as img containing the segmented images.
    
    Raises
    ------
    ValueError
        img has to have correct dimensions
    
    Notes
    -----
    Note that this function uses segmentation.preprocessing.background(). It causes an Error if
    gridX or gridY is chosen too large.

    """
    
    dim = len(img.shape)

    if dim == 2:
        bg = background(img, gridX, gridY)
        img_eq = image_histogram_equalization(img / bg, number_bins=256)
        _, img_masked = cv2.threshold(img_eq, threshold, 1.0, cv2.THRESH_BINARY)
        img_masked = img_masked == 1.0
        img_masked = morphology.remove_small_objects(img_masked, min_size=minSizeObjects)
        img_masked = np.asarray(img_masked, dtype=np.uint8)
        img_masked = cv2.dilate(img_masked, np.ones((3,3)))
        img_masked = morphology.remove_small_holes(img_masked, area_threshold=minSizeHoles)
        img_masked = np.asarray(img_masked, dtype=np.bool)
    
    elif dim == 3:
        img_masked = np.empty(img.shape, dtype=np.bool)
        for frame in range(img.shape[0]):
            img_masked[frame] = segmentImg(img[frame], gridX=gridX, gridY=gridY, threshold=threshold, minSizeObjects=minSizeObjects, minSizeHoles=minSizeHoles)
    
    elif dim == 4:
        img_masked = np.empty(img.shape, dtype=np.bool)
        for channel in range(img.shape[-1]):
            img_masked[:,:,:,channel] = segmentImg(img[:,:,:,channel], gridX=gridX, gridY=gridY, threshold=threshold, minSizeObjects=minSizeObjects, minSizeHoles=minSizeHoles)

    else:
        raise ValueError("img has wrong shape")

    return img_masked

