import numpy as np
import tensorflow as tf
import skimage
import cv2

from tensorflow.keras import layers
from tensorflow.keras import losses
from segmentation.preprocessing import decodeWeightFromY
from segmentation.preprocessing import getMinShape
try:
    from tqdm import tqdm_notebook
except:
    tqdm_notebook = range
    

def conv_block (input_tensor, num_filters):
    """ Block consisting of two convolutions as well as normalization/activation

    Parameters
    ----------
    input_tensor : tf.Tensor
    num_filters : int
        The number of convolutional filters
    
    Returns
    -------
    tf.Tensor

    Notes
    -----
    The output calculates as following:
    input_tensor -> Conv2D -> BatchNormalization -> Activation -> Dropout -> Conv2D -> BatchNormalization -> Activation

    """
    with tf.name_scope("ConvolutionBlock"):
        encoder = layers.Conv2D(num_filters, (3,3), padding="same")(input_tensor)
        encoder = layers.BatchNormalization()(encoder)
        encoder = layers.Activation("relu")(encoder)
        encoder = layers.Dropout(0.1)(encoder)
        encoder = layers.Conv2D(num_filters, (3,3), padding="same")(encoder)
        encoder = layers.BatchNormalization()(encoder)
        encoder = layers.Activation("relu")(encoder)
    return encoder

def encoder_block(input_tensor, num_filters, idNum):
    """ Encoder Block of UNET-Architecture

    Parameters
    ----------
    input_tensor : tf.Tensor
    num_filters : int
        The number of convolutional filters
    idNum : int, float, string
        An ID for tensorboard
    
    Returns
    -------
    encoder_pool : tf.Tensor
        Encoded Tensor maxpooled (using 2x2 mask)
    encoder : tf.Tensor
        input_tensor after encoding by convolution and max-pooling


    """
    with tf.name_scope("Encoder{}".format(idNum)):
        encoder = conv_block(input_tensor, num_filters)
        encoder_pool = layers.MaxPooling2D((2,2), strides=(2,2))(encoder)
    return encoder_pool, encoder

def decoder_block(input_tensor, concat_tensor, num_filters, idNum):
    """ Decoder Block of UNET-Architecture

    Parameters
    ----------
    input_tensor : tf.Tensor
    concat_tensor : tf.Tensor
        Last tensor before encoding to concate to upsampled input_tensor
    num_filters : int
        The number of convolutional filters
    idNum : int, float, string
        An ID for tensorboard
    
    Returns
    -------
    decoder : tf.Tensor

    Notes
    -----
    The output calculates as following:
    input_tensor -> Conv2DTranspose + concat_tensor -> BatchNormalization -> Activation -> Conv2D -> BatchNormalization -> Activation -> Conv2D -> BatchNormalization -> Activation

    """
    with tf.name_scope("Decoder{}".format(idNum)):
        decoder = layers.Conv2DTranspose(num_filters, (2,2), strides=(2,2), padding="same")(input_tensor)
        decoder = layers.concatenate([concat_tensor, decoder], axis=-1)
        decoder = layers.BatchNormalization()(decoder)
        decoder = layers.Activation("relu")(decoder)
        decoder = layers.Conv2D(num_filters, (3,3), padding="same")(decoder)
        decoder = layers.BatchNormalization()(decoder)
        decoder = layers.Activation("relu")(decoder)
        decoder = layers.Conv2D(num_filters, (3,3), padding="same")(decoder)
        decoder = layers.BatchNormalization()(decoder)
        decoder = layers.Activation("relu")(decoder)
    return decoder

def setupArchitecture(dataShape=(1056,1408,1), initialFilters=32):
    """ UNET Architecture

    Parameters
    ----------
    dataShape : tuple
        The shape of the data. Note that the training data needs an additional dimension for batches: trainingData.shape != (batchsize, *dataShape)
    initialFilters : int
        Number of Convolutional Filters for the first step
    
    Returns
    -------
    inputs : tf.keras.alyers.Input
        Input for Features
    outputs : tf.Tensor
        Output for predicted Labels

    """
    inputs = layers.Input(shape=dataShape)
    # 256
    encoder0_pool, encoder0 = encoder_block(inputs, initialFilters*2**0, 0)
    # 128
    encoder1_pool, encoder1 = encoder_block(encoder0_pool, initialFilters*2**1, 1)
    # 64
    encoder2_pool, encoder2 = encoder_block(encoder1_pool, initialFilters*2**2, 2)
    # 32
    encoder3_pool, encoder3 = encoder_block(encoder2_pool, initialFilters*2**3, 3)
    # 16
    encoder4_pool, encoder4 = encoder_block(encoder3_pool, initialFilters*2**4, 4)
    # 8
    center = conv_block(encoder4_pool, initialFilters*2**5)
    # center
    decoder4 = decoder_block(center, encoder4, initialFilters*2**4, 4)
    # 16
    decoder3 = decoder_block(decoder4, encoder3, initialFilters*2**3, 3)
    # 32
    decoder2 = decoder_block(decoder3, encoder2, initialFilters*2**2, 2)
    # 64
    decoder1 = decoder_block(decoder2, encoder1, initialFilters*2**1, 1)
    # 128
    decoder0 = decoder_block(decoder1, encoder0, initialFilters*2**0, 0)
    # 256
    outputs = layers.Conv2D(1, (1,1), activation="sigmoid")(decoder0)

    return inputs, outputs



def dice_loss(y_true, y_pred):
    """ dice loss function

    Parameters
    ----------
    y_true : tf.Tensor
        True Values
    y_pred : tf.Tensor
        Predicted Values
    
    Returns
    -------
    float
        The score achieved by the model, calculated via dice-loss

    """

    smooth = 1.
    # Flatten®
    y_true_f = tf.reshape(y_true, [-1])
    y_pred_f = tf.reshape(y_pred, [-1])
    intersection = tf.reduce_sum(y_true_f * y_pred_f)
    dice_coeff = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)

    loss = 1 - dice_coeff
    return loss

def bce_dice_loss(y_true, y_pred):
    loss = losses.binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    return loss

def customLossFunc(y_true, y_pred):
    """ custom loss function

    Parameters
    ----------
    y_true : tf.Tensor
        True Values
    y_pred : tf.Tensor
        Predicted Values
    
    Returns
    -------
    float
        Loss of the model
    
    Notes
    -----
    This Loss function combines the dice loss with weighted cross entropy

    """
    y_actual, weight = decodeWeightFromY(y_true)
    ce = tf.keras.backend.binary_crossentropy(y_actual, y_pred)
    wce = tf.multiply(ce, weight)
    
    loss1 = tf.keras.backend.mean(wce, axis=-1)
    loss2 = dice_loss(y_actual, y_pred)
    
    loss1 = (loss1 - 1) * 9 + 1
    return loss1 + loss2


def predict(model, stack, networkShape=None, divisions=5, useTQDM=True):
    """ Predicts the GT-Data with given Model and Stack of abritary shape

    Parameters
    ----------
    model : tf.keras.Model()
        A trained Model with input data shape (frames, y, x, channel)
    stack : array_like
        Data that the model uses to predict, shape: [y,x], [frame, y, x], [frame, y, x, channel]
    networkShape : tuple
        The imageshape required for the UNET model. If set to None, then it is assumed that stack fits the model
    divisions : np.int
        Number of convolution-components in UNET model
    useTQDM : bool
        Will display the progress in a notebook
    
    Returns
    -------
    array_like
        Prediction with the same shape as stack

    Raises
    ------
    ValueError
        Wrong dimension of data

    """
    stack = np.asarray(stack)
    stackDim = len(stack.shape)

    if stackDim==2:
        if networkShape is None:
            networkShape = stack.shape
        
        divider = 2 ** divisions
        heigth, width = stack.shape
        heigthPr, widthPr = getMinShape(networkShape, divisions)
        
        predImg = np.pad(stack, ((0, heigthPr-heigth), (0, widthPr-width)), mode="reflect")
        
        predImg = predImg.reshape(1,*predImg.shape,1)
        predImg = model.predict(predImg)
        
        return predImg[0,:heigth, :width,0]
    
    elif stackDim == 3:
        frames = stack.shape[0]
        ret = np.zeros(shape=stack.shape, dtype=stack.dtype)
        
        if useTQDM:
            custRange = lambda x: tqdm_notebook(range(x))
        else:
            custRange = lambda x: range(x)
            
        for i in custRange(frames):
            ret[i] = predict(model=model, stack=stack[i], networkShape=networkShape, divisions=divisions, useTQDM=useTQDM)

        return ret
    
    elif stackDim == 4:
        channels = stack.shape[-1]
        ret = np.zeros(shape=stack.shape, dtype=stack.dtype)
        
        if useTQDM:
            custRange = lambda x: tqdm_notebook(range(x))
        else:
            custRange = lambda x: range(x)
            
        for i in custRange(channels):
            ret[:,:,:,i] = predict(model=model, stack=stack[:,:,:,i], networkShape=networkShape, divisions=divisions, useTQDM=useTQDM)

        return ret

    else:
        raise ValueError("Wrond Dimension: {}".format(stackDim))

def Model(inputs, outputs, loadWeightsFrom=None, optimizer="adam", loss=customLossFunc):
    """ Wrapper returning the model, plus loading weights (optional)

    Parameters
    ----------
    inputs : tf.keras.Inputs
        Inputs of the Model
    outputs : tf.keras.layers.xxx
        Layer for output
    loadWeightsFrom : string
        Loads weights from this file, if set to None then no weights are loaded
    optimizer : string
        The optimizer used for training
    loss : lambda y_true, y_pred: function(y_true, y_pred)
        Loss function to optimize

    Returns
    -------
    tf.keras.Model
        A configured Model with optionally loaded weights

    """
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    model.compile(optimizer=optimizer, loss=loss)

    if loadWeightsFrom:
        model.load_weights(loadWeightsFrom)
    
    return model

def validate(model, valImgX, valImgY, eps=0.1):
    """ Validates the training by calculating overlap of predicted labels and true labels

    Parameters
    ----------
    model : tf.keras.Model
        Segmention Model, eg. UNET
    valImgX : array_like
        Features of validationdata
    valImgY : array_like
        True labels of validationdata
    eps : float
        Accuracy trying to score
    
    Returns
    -------
    array_like
        The relative amount of pixels appeared in the true-labels but not by the model-prediction
    
    array_like
        The relative amount of pixels appeared in the model-prediction but not in the true-labels

    """
    valImgY /= np.max(valImgY)
    predImgY = predict2(predict(model, valImgX, shape=(1056, 1408)), 0.2)

    compR = np.zeros(valImgX.shape, dtype=np.uint8)
    
    compR[np.abs(valImgY - predImgY) < eps] = 1
    compR = 1 - compR
    compB = compR.copy()

    compR[valImgY > predImgY] = 0
    compB[valImgY < predImgY] = 0
    
    compR = np.asarray(compR, dtype=np.float64)
    compB = np.asarray(compB, dtype=np.float64)
    
    divR = compR.size * np.max(compR) * valImgX.shape[0]
    divB = compB.size * np.max(compB) * valImgY.shape[0]
    
    compR = compR / divR if divR else 0
    compB = compB / divB if divB else 0
    
    return np.sum(compB), np.sum(compR)

