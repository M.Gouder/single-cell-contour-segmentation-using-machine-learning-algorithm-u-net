import os
import numpy as np
import tensorflow as tf

from imgaug import augmenters as iaa
from segmentation.pyama import stack
from operator import itemgetter
from scipy import interpolate
try:
    from tqdm import tqdm_notebook
except:
    tqdm_notebook = range
    
def findArgMax(img, bins=1000):
    """ Find's the Value which appears most

    Parameters
    ----------
    img : array_like
        The image to find the most common pixelvalue, must be 2-dim
    bins : int
        The number of binaries to calculate the histogram with
    
    Returns
    -------
    np.float
        Returns the most common value in the img

    Notes
    -----
    A small variance of the brightest pixels are ignored for the calculation

    """
    hist, bin_edges = np.histogram(img, bins)
    binCenters = np.mean([bin_edges[:-1],bin_edges[1:]], axis=0)
    return binCenters[np.argmax(hist[:-10])]

def background(img, gridX, gridY, calcAverage=findArgMax, interpolationKind="linear", paddingMode="edge"):
    """ Approximates the background while neglecting noise and other local features

    Parameters
    ----------
    img : array_like
        The image to calculate the background of, must be 2-dim
    gridX : np.uint
        Sets number of vertical sections used for background interpolation
    gridY : np.uint
        sets number of horizontal sections used for background interpolation
    calcAverage : function
        A function to calculate the background value
    interpolationKind : str or int
        Specifies the kind of interpolation: 'linear', 'cubic', 'quintic'
    paddingMode : str or function
        The method to calculate the edges
    
    Returns
    -------
    array_like
        The approximated background

    Raises
    ------
    ValueError
        If the Gridsize is too large

    Notes
    -----
    When using other interpolationKind than 'linear', artefacts appear at the edges.
    It causes an Error if gridX or gridY is chosen too large.

    """
    imgHeight, imgWidth = img.shape
    stepX, stepY = int(imgWidth / gridX), int(imgHeight / gridY)
    if (stepX <= 1) or (stepY <= 1):
        raise ValueError("Gridsize too large")
    halfStepX, halfStepY = int(stepX/2), int(stepY/2)

    coordsX = np.arange(halfStepX, imgWidth, stepX)
    coordsY = np.arange(halfStepY, imgHeight, stepY)
    valZ = np.zeros((gridY, gridX), dtype=img.dtype)

    for y in range(gridY):
        for x in range(gridX):
            ROI = img[y*stepY:(y+1)*stepY, x*stepX:(x+1)*stepX]
            valZ[y,x] = calcAverage(ROI)


    coordsX_new = np.arange(imgWidth, dtype=np.uint16)[halfStepX:-halfStepX]
    coordsY_new = np.arange(imgHeight, dtype=np.uint16)[halfStepY:-halfStepY]

    f = interpolate.interp2d(coordsX, coordsY, valZ, kind=interpolationKind)
    background = f(coordsX_new, coordsY_new)
    background = np.pad(background, ((halfStepY, halfStepY),(halfStepX, halfStepX)), paddingMode)

    return background

def image_histogram_equalization(image, number_bins=256):
    """ Increases the histogram to the extreme

    Parameters
    ----------
    image : array_like
        The input to increaste the contrast, mus tbe 2-dim
    number_bins : np.int
        Number of portions of the histogram.
        The lower this number, the more bright values will be clipped after increasing contrast
    
    Returns
    -------
    array_like
        The image with increased contrast
        Has the same shape as the inputimage

    """
    image_histogram, bins = np.histogram(image.flatten(), number_bins, density=True)
    cdf = image_histogram.cumsum()
    cdf = cdf / cdf[-1]
    image_equalized = np.interp(image.flatten(), bins[:-1], cdf)
    if np.issubdtype(image.dtype, np.inexact):
        return np.asarray(image_equalized.reshape(image.shape), image.dtype)
    else:
        return np.asarray(image_equalized.reshape(image.shape)*np.iinfo(image.dtype).max, image.dtype)

def normalizeBackground(arr, gridX=15, gridY=10, useTQDM=False):
    """ Increases contrast and removes unhomogenous background

    Parameters
    ----------
    arr : array_like
        input image or stack of images with shape [y,x], [frame,y,x], [frame, y, x, channel]
    gridX : np.uint
        Sets number of vertical sections used for background interpolation
    gridY : np.uint
        sets number of horizontal sections used for background interpolation
    useTQDM : bool
        When using a notebook, the progress can be shown in realtime
    
    Returns
    -------
    array_like
        Returns arr, but with the background removed and an increased contrast.

    Notes
    -----
    useTQDM is not in all browsers available, was tested with Google Chrome.
    Note that this function uses segmentation.preprocessing.background(). It causes an Error if
    gridX or gridY is chosen too large.

    """
    dim = len(arr.shape)
    
    if dim == 2:
        ret = image_histogram_equalization(arr/background(arr,gridX,gridY))
    elif dim == 3:
        ret = np.empty(arr.shape)
        if useTQDM:
            custRange = lambda x: tqdm_notebook(range(x))
        else:
            custRange = lambda x: range(x)
        for frame in custRange(arr.shape[0]):
            ret[frame] = normalizeBackground(arr[frame], gridX=gridX, gridY=gridY, useTQDM=useTQDM)
    elif dim == 4:
        ret = np.empty(arr.shape)
        for channel in range(arr.shape[-1]):
            ret[:,:,:,channel] = normalizeBackground(arr[:,:,:,channel], gridX=gridX, gridY=gridY, useTQDM=useTQDM)
    
    return ret


# following Types for img allowed:
# img = [y,x]
# img = [frame, y,x]
# img = [frame, y,x, channel]
def fitImageToNodes(img, divisions=5, mode="reflect", norming=True, useTQDM=False, dtype=np.float16):
    """ Adjusts the shape of img to fit the UNET-model
    img : array_like
        img or stack to adjust shape of, shapes like [y,x], [frame,y,x], [frame, y, x, channel] are allowed
    divisions : np.int
        Number of convolution-components in UNET model
    mode :  str or function
        Specifies the method to pad the edges with
    norming : bool
        If set to true, the output is scaled to 1
    useTQDM : bool
        When using a notebook, the progress can be shown in realtime
    dtype : np.dtype
        The dtype for the output
    
    Returns
    -------
    array_like
        img, up-scaled so it fits any UNET-model. For increasing the shape, the edges are padded

    Raises
    ------
    ValueError
        When img is empty

    Notes
    -----
    useTQDM is not in all browsers available, was tested with Google Chrome.
    With this function, the original img is in center of the output. The edges are being padded.

    """
    if not img.any():
        raise ValueError("Image empty")
    
    if len(img.shape) == 2:
        _width, _heigth = img.shape

        divider = 2**divisions

        _difX = (np.ceil(_width/divider) * divider - _width) / 2
        _difY = (np.ceil(_heigth/divider) * divider - _heigth) / 2
        
        _hfPxX = np.int(np.ceil(_difX-np.floor(_difX)))
        _hfPxY = np.int(np.ceil(_difY-np.floor(_difY)))
        
        _difX = np.int(_difX)
        _difY = np.int(_difY)
        
        pad_width = (
            (_difX, _difX+_hfPxX),
            (_difY, _difY+_hfPxY)
        )

        ret = np.asarray( np.pad(img, pad_width=pad_width, mode=mode), dtype=dtype )
        if norming:
            ret = ret / np.max(ret)
        _heigth, _width = ret.shape

        ret = ret.reshape(1, _heigth, _width, 1)
        ret = np.asarray(ret, dtype=dtype)
        return ret
    
    elif len(img.shape) == 3:
        frames = img.shape[0]
        ret = np.array([], dtype=dtype)
        
        if useTQDM:
            custRange = lambda x: tqdm_notebook(range(x))
        else:
            custRange = lambda x: range(x)
            
        for i in custRange(frames):
            if not ret.any():
                ret = fitImageToNodes(img[i], divisions=divisions, mode=mode, norming=norming, dtype=dtype)
            else:
                ret = np.concatenate((ret, fitImageToNodes(img[i], divisions=divisions, mode=mode, norming=norming, dtype=dtype)), axis=0)
        return ret
    
    elif len(img.shape) == 4:
        channels = img.shape[-1]
        ret = np.array([], dtype=dtype)
        
        for i in range(channels):
            if not ret.any():
                ret = fitImageToNodes(img[:,:,:,i], divisions=divisions, mode=mode, norming=norming, dtype=dtype)
            else:
                ret = np.concatenate((ret, fitImageToNodes(img[:,:,:,i], divisions=divisions, mode=mode, norming=norming, dtype=dtype)), axis=-1)
        return ret
    
    else:
        raise ValueError("img has wrong dimension: {}".format(len(img.shape)))

def getMinShape(shape, divisions=5):
    """ Next smallest shape so it'll fit a UNET-model

    Parameters
    ----------
    shape : tuple
        The shape of the image that should fit the UNET-model. (y,x), (frames,y,x), (frames,y,x,channels) are allowed
    divisions : np.int
        Number of convolution-components in UNET model
    
    Returns
    -------
    tuple
        The next smallest shape so the image would be fitable for a UNET-model

    Raises
    ------
    ValueError
        shape has wrong dimension
    
    """
    if divisions < 0:
        raise ValueError("divisions too small: {}".format(divisions))

    dim = len(shape)
    
    if dim == 2:
        heigth, width = shape
    elif dim == 3:
        frames, heigth, width = shape
    elif dim == 4:
        frames, heigth, width, channels = shape
    else:
        raise ValueError("Wrong dimension of shape")
        
    divider = 2 ** divisions
    widthPr = np.int(np.ceil(width/divider) * divider)
    heigthPr = np.int(np.ceil(heigth/divider) * divider)
    print(widthPr, heigthPr)
    if dim == 2:
        return (heigthPr, widthPr)
    if dim == 3:
        return (frames, heigthPr, widthPr)
    if dim == 4:
        return (frames, heigthPr, widthPr, channels)


def fitImageToNodesEdge(img, divisions=5, mode="reflect", norming=True, useTQDM=False, dtype=np.float16):
    """ Adjusts the shape of img to fit the UNET-model
    img : array_like
        img or stack to adjust shape of, shapes like [y,x], [frame,y,x], [frame, y, x, channel] are allowed
    divisions : np.int
        Number of convolution-components in UNET model
    mode :  str or function
        Specifies the method to pad the edges with
    norming : bool
        If set to true, the output is scaled to 1
    useTQDM : bool
        When using a notebook, the progress can be shown in realtime
    dtype : np.dtype
        The dtype for the output
    
    Returns
    -------
    array_like
        img, up-scaled so it fits any UNET-model. For increasing the shape, the edges are padded

    Notes
    -----
    useTQDM is not in all browsers available, was tested with Google Chrome Version 75.0.3770.100.
    With this function, the original img is in not in the center of the output, but in the top-left corner!
    The edges are being padded. This method is much more efficient than fitImageToNodes().
    The performance fo this function drops significantly if img is too small.

    """
    dim = len(img.shape)
    
    if dim < 2:
        raise ValueError("Got too few dimensions: dim={}".format(dim))
    
    elif dim == 2:
        shape = img.shape
        minShape = getMinShape(shape)
        difH = minShape[0] - shape[0]
        difW = minShape[1] - shape[1]
        
        if (difH > shape[0]) or (difW > shape[1]):
            ret = np.pad(img, ((0,difH),(0,difW)), mode)
            ret = np.asarray(ret, dtype)
        else:
            ret = np.empty(minShape, dtype)
            ret[:shape[0],:shape[1]] = img
            ret[shape[0]:,:shape[1]] = img[-1:-(minShape[0]-shape[0]+1):-1,:]
            ret[:,shape[1]:] = ret[:, shape[1]:shape[1]-(minShape[1]-shape[1]):-1]
            
        if norming:
            ret /= np.max(ret)
    
    elif dim == 3:
        frames = img.shape[0]
        ret = np.array([], dtype=dtype)
        
        shape = img.shape
        minShape = getMinShape(shape)
        difH = minShape[1] - shape[1]
        difW = minShape[2] - shape[2]
        
        if (difH > shape[1]) or (difW > shape[2]):
            print("slow")
            if useTQDM:
                custRange = lambda x: tqdm_notebook(range(x))
            else:
                custRange = lambda x: range(x)

            for i in custRange(frames):
                if not ret.any():
                    ret = fitImageToNodesEdge(img[i], divisions=divisions, mode=mode, norming=norming, dtype=dtype)
                    ret = np.expand_dims(ret,0)
                else:
                    tmp = np.expand_dims(fitImageToNodesEdge(img[i],divisions=divisions, mode=mode, norming=norming, dtype=dtype), axis=0)
                    ret = np.concatenate((ret, tmp), axis=0)
        
        else:
            ret = np.empty(minShape, dtype)
            ret[:, :shape[1], :shape[2]] = img
            ret[:, shape[1]:, :shape[2]] = img[:, -1:-(minShape[1]-shape[1]+1):-1,:]
            ret[:, :, shape[2]:] = ret[:, :, shape[2]:shape[2]-(minShape[2]-shape[2]):-1]
    
    elif dim == 4:
        channels = img.shape[-1]
        ret = np.array([], dtype=dtype)
        
        for i in range(channels):
            if not ret.any():
                ret = fitImageToNodesEdge(img[:,:,:,i], divisions=divisions, mode=mode, norming=norming, dtype=dtype)
                ret = np.expand_dims(ret,-1)
            else:
                tmp = np.expand_dims(fitImageToNodesEdge(img[:,:,:,i],divisions=divisions, mode=mode, norming=norming, dtype=dtype), axis=-1)
                ret = np.concatenate((ret, tmp), axis=-1)
    
    else:
        raise ValueError("Got too many dimensions: dim={}".format(dim))
    return ret

def filename(file):
    """ Filename without extansion

    Parameters
    ----------
    file : string
        full or relative path to a file
    
    Returns
    -------
    string
        only the filename without the path or the extansion of the data type
    
    Examples
    --------
    >>> segmentation.preprocessing.filename("./path/to/file/filename.foo")
    str("filename")

    """
    return os.path.splitext(os.path.basename(file))[0]

def loadFile(file):
    """ loads file in memmap

    Parameters
    ----------
    file : string
        path to file
        file can be of type '.tif', '.npy', '.csv'
    
    Returns
    -------
    np.memmap
        Data stored in file
    
    Raises
    ------
    ValueError
        Unkown Datatype
    
    Notes
    -----
    When file is of type '.csv', it must have delimiter ','

    """

    if os.path.splitext(file)[1] == ".tif":
        return stack.Stack(file).img
    elif os.path.splitext(file)[1] == ".npy":
        return np.load(file, mmap_mode="r+")
    elif os.path.splitext(file)[1] == ".csv":
        newFile = os.path.splitext(file)[0]
        np.save(newFile, np.loadtxt(file, delimiter=","))
        return np.load(newFile + ".npy", mmap_mode="r+")
    else:
        raise ValueError("Unkown Datatype")

def encodeWeightToY(y_true, weight):
    """ Encodes the Weight to the Label

    Parameters
    ----------
    y_true : array_like
        True Label, Values should be in {0, 1}
    weight : array_like
        The weight that should be encoded with the label data
        Values should be in range [0, 1)
    
    Returns
    -------
    array_like
        Labels and Weights combined in one array with shape y_true.shape
        This can be passed to keras.Model().fit

    """
    return y_true + weight / 2.
    
def decodeWeightFromY(y_true, useTF=True):
    """ Decodes the Weight from the Label

    Parameters
    ----------
    y_true : array_like
        The Label which contains the true label as well as the weights encoded.
    
    Returns
    -------
    y_actual : array_like
        The actual label data
    weight : array_like
        The weights ranging from 0 to 1

    """
    if useTF:
        y_actual = tf.math.floor(y_true)
    else:
        y_actual = np.floor(y_true)
    weight = y_true - y_actual
    return y_actual, 1+weight

def imageGenerator(imageData, weight_args, indices=None, fitToNodes=False, augmenter=None, useDistance=True, batch_size=1, seed=None):
    """ Generator that yields trainingdata

    Parameters
    ----------
    imageData : dict
        Dictionary that contains all relevant data as array_like values, with shapes [frame, y, x, channel]
        imageData['dataRaw'] <-> features
        imageData['dataGT'] <-> labels
        imageData['d1'] <-> distance to nearest cell (optional)
        imageData['d2'] <-> distance to second nearest cell (optional)
    weight_args : dict
        Dictionary that contains tweakable parameters when using distance maps (weighted labels)
        weight_args['w0'] <-> overall multiplification factor for weights
        weight_args['sigma] <-> sharpens the edges between cells
    indices : list
        Specifies which images should be used as featers, labels, ...
        If set to None, then all images are being used
    fitToNodes : bool
        If set to True, then all images will be fitted for the UNET network.
        Set this to False if images do already fit to improve performance
    augmenter : function
        If not None, then the training data get augmented via augmenter
    useDistance : bool
        If set to True and imageData['d1], imageData['d2'] is provided, then it will calculate a weightmap with priority to edges of cells. See formula in Notes
    batch_size : int
        Size of the batch to be returned
    seed : int
        Specifies seed for augmenter. If set to None, then it chooses a random seed
    
    Yields
    ------
    retFeatures : array_like
        Feature Map with shape [batchsize, y, x, ...]
    
    retLabels : array_like
        Feature Map plus Weight Map encoded with shape [batchsize, y, x, ...]
    
    retMeta : dict
        Dictionary containing information of the index of which the image was chosen from imageData['dataRaw']/imageData['dataGT'] as well as it corresponding frame index

    Notes
    -----
    This image generator selects multiple frames in multiple stacks/images from imageData['dataRaw']. Both selections are done by random.
    After calculating the weights by
    .. math::
        w(x)=w_c(x)+w_0\cdot \text{exp} \left( - \frac{\left( d_1(x) + d_2(x) \right)^2}{2 \sigma^2} \right),
    the output will get augmented and encoded so labels and weights are in the same array.

    The number of images yielded is set by batch_size.


    """
    import sys
    
    if indices is None:
        indices = np.arange(len(imageData['dataRaw']))
    
    dataX = itemgetter(*indices)(imageData['dataRaw'])
    dataY = itemgetter(*indices)(imageData['dataGT'])

    dataD1 = imageData.get('d1')
    dataD2 = imageData.get('d2')

    if (dataD1 != None) and (dataD2 != None) and (len(dataD1)) and (len(dataD2)):
        dataD1 = itemgetter(*indices)(dataD1)
        dataD2 = itemgetter(*indices)(dataD2)
    
    useDistance = useDistance and (dataD1 != None) and (dataD2 != None)
    height, width = dataX[0].shape[1:3]

    if augmenter==None:
        augmenter = lambda data: data

    def getLabel(index, useDistance=useDistance):
        
        _y = dataY[index]
        _d1 = dataD1[index]
        _d2 = dataD2[index]

        if useDistance:
            _w = 2-_y + weight_args["w0"]*np.exp(- np.power(_d1+_d2,2)/(2*np.square(weight_args["sigma"])))
            _w /= np.max(_w)
        else:
            _w = np.ones(_y.shape)
        
        return encodeWeightToY(_y, _w)
    
    
    # data must be of type float 
    def inverter(data, p=0.5):
        if np.random.random() < p:
            return 1-data
        return data

    while True:
        randomPics = np.random.randint(0, len(dataX), batch_size)
        
        retFeatures = []
        retLabels = []
        retMeta = []

        for picIndex in randomPics:
            frameIndex = np.random.randint(0, len(dataX[picIndex]))
            retFeatures.append(dataX[picIndex][frameIndex])
            retLabels.append(dataY[picIndex][frameIndex])
            retMeta.append({"index":indices[picIndex], "frame":frameIndex})
        
        if seed is None:
            seed = np.random.randint(np.uint32(-1))
        
        retFeatures = augmenter(np.asarray(retFeatures, dtype=np.float64), seed)
        retLabels = augmenter(np.asarray(retLabels, dtype=np.float32), seed)
        
        if fitToNodes:
            retFeatures = fitImageToNodesEdge(retFeatures[:,:,:],5, dtype=np.float64)
            retLabels = fitImageToNodesEdge(retLabels[:,:,:],5, dtype=np.float64)
        else:
            retFeatures = retFeatures[:,:,:,0]
            retLabels = retLabels[:,:,:,0]
        
        yield retFeatures, retLabels, retMeta

def fitGenerator(**kwargs):
    """ In general a copy of segmentation.imageGenerator(), but without Metadata so it can be used for training a keras.Model()

    Parameters
    ----------
    All parameters can be found at segmentation.imageGenerator()

    Yields
    ------
    retFeatures : array_like
        Feature Map with shape [batchsize, y, x, ...]
    
    retLabels : array_like
        Feature Map plus Weight Map encoded with shape [batchsize, y, x, ...]


    """
    gen = imageGenerator(**kwargs)
    while True:
        yield next(gen)[0:2]

def augmenter(data, seed=None):
    """ Image augmentator

    Parameters
    ----------
    data : array_like
        The data that should get augmentated
    seed : np.uint
        The seed for the augmentator. If set to None, then the seed well be chosen by random.

    Returns
    -------
    array_like
        data augmentated

    Notes
    -----
    This augmentator is meant to be used on images containing biological cells. It will flip the image horizontally
    and vertically by chance of 50% each. It also performs an Affine-Transformation which scales the image by a factor of
    0.7 to 1.7, translates the image by 20% in x-/y-direction, rotates the image by 0 to 360 degrees, shears the image by 0.3 degrees.
    If there arise any exposed edges, then the image will be padded by "reflect"-mode.

    """
    if not seed:
        seed = np.random.randint(np.uint32(-1))
    return iaa.Sequential([
        iaa.Fliplr(0.5, random_state=seed),
        iaa.Flipud(0.5, random_state=seed),
        iaa.Affine(scale=(0.7, 1.7), translate_percent=0.2, rotate=(0, 360),
                    shear=0.3, mode="reflect", random_state=seed)
    ])(images=data)


