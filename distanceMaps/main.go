package main

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"io/ioutil"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/akamensky/argparse"
	"gocv.io/x/gocv"
)

type Vertex struct {
	X, Y int
}

func DistanceSquare(v, w *Vertex) float64 {
	return math.Sqrt(float64((v.X-w.X)*(v.X-w.X) + (v.Y-w.Y)*(v.Y-w.Y)))
}

func handleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func findNonZero(img [][]float64) []Vertex {
	res := make([]Vertex, 0)

	for y, row := range img {
		for x, val := range row {
			if val > 0 {
				res = append(res, Vertex{y, x})
			}
		}
	}
	return res
}

func loadContoursFromTIFF(pathToFile string) ([][][]float64, error) {

	img := gocv.IMRead(pathToFile, gocv.IMReadGrayScale)

	thresh := gocv.NewMat()
	gocv.Threshold(img, &thresh, 127, 255, gocv.ThresholdBinary)

	contours := gocv.FindContours(thresh, gocv.RetrievalTree, gocv.ChainApproxSimple)

	res := make([][][]float64, len(contours))

	for contIndex, cont := range contours[1:] {
		// fmt.Println(cont[0] == image.Point{0, 0})

		contImg := thresh
		contImg.MultiplyFloat(0)

		contAr := make([][]image.Point, 1)
		contAr[0] = cont
		gocv.DrawContours(&contImg, contAr, -1, color.RGBA{0, 0, 255, 0}, 1)

		nonZero := gocv.NewMat()
		gocv.FindNonZero(contImg, &nonZero)

		res[contIndex] = make([][]float64, nonZero.Rows())
		for voxIn := 0; voxIn < nonZero.Rows(); voxIn++ {
			res[contIndex][voxIn] = make([]float64, 2)
			res[contIndex][voxIn][0] = float64(nonZero.GetVeciAt(voxIn, 0)[0])
			res[contIndex][voxIn][1] = float64(nonZero.GetVeciAt(voxIn, 0)[1])
		}

	}

	return res, nil
}

// DIM1: index of contour | DIM2, DIM3: X, Y - coords
// expecting as input data the coords of white pixels
func loadContoursFromNonZero(pathToDir string, sep string) ([][][]float64, error) {
	files, err := ioutil.ReadDir(pathToDir)
	if err != nil {
		return nil, err
	}
	fileNames := make([]string, len(files))
	for i, f := range files {
		fileNames[i] = pathToDir + f.Name()
	}

	res := make([][][]float64, len(fileNames))

	for contourIndex, contourFileName := range fileNames {

		contourFile, err := os.Open(contourFileName)
		if err != nil {
			return nil, err
		}
		defer contourFile.Close()

		var lines []string
		scanner := bufio.NewScanner(contourFile)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}

		res[contourIndex] = make([][]float64, len(lines))
		for i := range res[contourIndex] {
			res[contourIndex][i] = make([]float64, 2)
			s := strings.Split(lines[i], sep)

			for coordIndex, val := range s {
				n, err := strconv.ParseFloat(val, 64)
				if err != nil {
					return nil, err
				}
				res[contourIndex][i][coordIndex] = n
			}
		}
	}
	return res, nil
}

// width, height: size of result
func distMaps(contours [][][]float64, width int, height int, threads int, printProgress bool) (d1 [][]float64, d2 [][]float64) {

	_findDistanceToContour := func(contour [][]float64, target Vertex) float64 {

		minDistSquare := math.MaxFloat64

		for _, vert := range contour {

			_difX := vert[0] - float64(target.X)
			_difY := vert[1] - float64(target.Y)

			_difX *= _difX
			_difY *= _difY

			if d := _difX + _difY; d < minDistSquare {
				minDistSquare = d
			}
		}
		return math.Sqrt(minDistSquare)
	}

	_findD1D2 := func(contours [][][]float64, target Vertex) (float64, float64) {
		_d1, _d2 := math.MaxFloat64, math.MaxFloat64
		for _, contour := range contours {
			_d := _findDistanceToContour(contour, target)
			if _d < _d1 {
				_d2 = _d1
				_d1 = _d
			} else if _d < _d2 {
				_d2 = _d
			}
		}
		return _d1, _d2
	}

	_fillArr := func(d1 [][]float64, d2 [][]float64, contours [][][]float64, coords Vertex, counter *int, done chan bool) {
		d1[coords.Y][coords.X], d2[coords.Y][coords.X] = _findD1D2(contours, coords)
		*counter++
		done <- true
	}

	_printProgrss := func(timeStart *time.Time, finished *bool, pixelCnt *int, pixelCntMax *int) {
		for !(*finished) {
			elapsed := time.Now().Sub(*timeStart)
			fmt.Printf("Calculated Pixels: %d\tRelative Progress: %f\tPixelCnt/time (1/ms): %f\n",
				*pixelCnt, float64(*pixelCnt)/float64(*pixelCntMax), float64(*pixelCnt*1000000)/(float64(elapsed)))
			time.Sleep(time.Second * 1)
		}
	}

	cnt := 0
	cntMax := width * height
	finished := false
	done := make(chan bool, threads)
	for i := 0; i < threads; i++ {
		done <- true
	}

	d1 = make([][]float64, height)
	d2 = make([][]float64, height)

	timeStart := time.Now()
	if printProgress {
		go _printProgrss(&timeStart, &finished, &cnt, &cntMax)
	}
	for index_y := 0; index_y < height; index_y++ {

		d1[index_y] = make([]float64, width)
		d2[index_y] = make([]float64, width)

		for index_x, _ := range d1[index_y] {
			<-done
			go _fillArr(d1, d2, contours, Vertex{index_x, index_y}, &cnt, done)
			// d1[index_y][index_x], d2[index_y][index_x] = _findD1D2(contours, Vertex{index_x, index_y})
		}
	}
	// This is not elegant!!
	if d2[0][0] == math.MaxFloat64 {
		d2 = nil
	}
	finished = true
	elapsed := time.Now().Sub(timeStart)
	fmt.Printf("Total Time (sec): %f\n", float64(elapsed)/1000000000.)

	return
}

func overlayInputToDistMap(pathToFile string, d1 [][]float64) {
	img := gocv.IMRead(pathToFile, gocv.IMReadGrayScale)

	RowMax := math.Min(float64(len(d1)), float64(img.Rows()))
	ColMax := math.Min(float64(len(d1[0])), float64(img.Cols()))

	for indY := 0; indY < int(RowMax); indY++ {
		for indX := 0; indX < int(ColMax); indX++ {
			if img.GetUCharAt(indY, indX) == 0 {
				d1[indY][indX] = 0.
			}
		}
	}

	// fmt.Println(img.Rows(), img.Cols(), img.Channels())
}

func saveFloatMap(data [][]float64, sep string, path string) {
	ln := len(data)
	lineStrings := make([]string, ln)
	for i, row := range data {
		str := make([]string, len(row))
		for i, val := range row {
			str[i] = strconv.FormatFloat(val, 'f', 10, 64)
		}
		lineStrings[i] = strings.Join(str, sep)
		if i < ln-1 {
			lineStrings[i] += "\n"
		}
	}
	file, _ := os.Create(path)
	defer file.Close()
	writer := bufio.NewWriter(file)
	for _, row := range lineStrings {
		writer.WriteString(row)
	}
	writer.Flush()
}

func calcMapsFromDir(pathToFiles string, pathToResultDir string, threads int, logProgress string) {
	files, _ := filepath.Glob(pathToFiles)
	for _, file := range files {
		_, filename := filepath.Split(file)
		if logProgress == "minimal" || logProgress == "full" {
			fmt.Println("Processing", filename, "...")
		}
		filename = strings.Split(filename, ".")[0]
		contours, _ := loadContoursFromTIFF(file)
		img := gocv.IMRead(file, gocv.IMReadGrayScale)
		d1, d2 := distMaps(contours, img.Cols(), img.Rows(), int(threads), logProgress == "full")
		overlayInputToDistMap(file, d1)

		saveFloatMap(d1, ",", pathToResultDir+filename+"_d1.csv")
		saveFloatMap(d2, ",", pathToResultDir+filename+"_d2.csv")
	}
}

func main() {

	parser := argparse.NewParser("default", "Programm for calculating distancemap to nearest/second-nearest cell")

	// Creating string argument with default value and required set to false
	threads := parser.Int("t", "threads", &argparse.Options{Required: false, Help: "Number of Threads for Multi-Threading", Default: 1})
	inputFilePath := parser.String("i", "inputFilePath", &argparse.Options{Required: false, Help: "The path to all files", Default: "./data/"})
	fileType := parser.String("d", "dataType", &argparse.Options{Required: false, Help: "Data Type", Default: "tif"})
	outputdirpath := parser.String("o", "outputDirPath", &argparse.Options{Required: false, Help: "Path where the results will be saved", Default: "./results/"})
	logProgressOptions := []string{"disable", "minimal", "full"}
	logProgress := parser.Selector("l", "logProgress", logProgressOptions, &argparse.Options{Required: false, Help: "Options for logging", Default: "full"})
	// Parse input
	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}
	if (*outputdirpath)[len(*outputdirpath)-1] != '/' {
		*outputdirpath += "/"
	}
	calcMapsFromDir(*inputFilePath+"*."+*fileType, *outputdirpath, *threads, *logProgress)
}
