#! /usr/bin/env python3
"""Load segmentation data as numpy array and/or
save it as TIFF file.

For use as a module, you can use the
`load` and `export` functions.

For use as a standalone program, execute:
  python load_segmented.py path/to/file.npz
"""
import os
import numpy as np
import tifffile

def load(fn):
    """Load a numpy array from a .npy or .npz file.
    
    The name of the input file is `fn`.
    The loaded numpy array is returned.
    If the input file is a .npz file and contains several
    arrays, only the first array is returned.
    """
    ext = os.path.splitext(fn)[-1]
    if ext == '.npy':
        return np.load(fn)
    elif ext == '.npz':
        with np.load(fn) as seg_file:
            return next(iter(seg_file.values())).astype(np.uint16, casting='unsafe')

def export(fn, arr, imagej=True):
    """Export a numpy array to a TIFF file.
    
    The numpy array `arr` is exported as a TIFF file
    with the file name `fn`.
    The option `imagej` indicates whether the TIFF file
    should be in the imagej format.
    """
    tifffile.imwrite(fn, arr, imagej=imagej)

def _get_export_name(fn):
    """Internal: generate TIFF file name from np[yz] file name"""
    return ".".join((os.path.splitext(fn)[0], "tiff"))

if __name__ == '__main__':
    import sys
    try:
        fn = sys.argv[1]
    except IndexError:
        raise ValueError("No input file given.")
    export(_get_export_name(fn), load(fn))
