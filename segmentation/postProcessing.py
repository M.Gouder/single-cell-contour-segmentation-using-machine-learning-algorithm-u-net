import numpy as np
import skimage
import cv2
from segmentation.masks import circle


def postProcessPrediction(arr, percentageThreshold=0.2, holesMinSizes=200, objectsMinSize=300, dilateMask=None):
    """ Postprocessing to obtain true ground-truth data
    
    Parameters
    ----------
    arr : array_like
        The prediction to postprocess, shape: [y,x], [frame, y, x], [frame, y, x, channel]
    percentageThreshold : np.float
        arr > percentageThreshold -> 1
        arr <= precentageThreshold -> 0
    holesMinSize : int
        The minimal allowed size for holes
    objectsMinSize : int
        The minimal allowed size for objects
    dilateMask : array_like
        Dilation for smoothing the edges and making the Model more optimistic
    
    Returns
    -------
    array_like
        True ground-truth data processed.

    Raises
    ------
    ValueError
        Wrong dimension for data
    
    """
    if dilateMask is None:
        dilateMask = circle(2)
    dim = len(arr.shape)
    
    if dim == 2:
        arr = np.array(arr) > percentageThreshold
        arr = skimage.morphology.remove_small_holes(arr, holesMinSizes)
        arr = skimage.morphology.remove_small_objects(arr, objectsMinSize)
        arr = np.uint8(arr)
        arr = cv2.dilate(arr, dilateMask)
        return arr
    
    elif dim == 3:
        ret = np.empty(arr.shape, np.uint8)
        for frame in range(arr.shape[0]):
            ret[frame] = postProcessPrediction(arr[frame], percentageThreshold)
        return ret
    
    elif dim == 4:
        ret = np.empty(arr.shape, np.uint8)
        for channel in range(arr.shape[-1]):
            ret[:,:,:,channel] = postProcessPrediction(arr[:,:,:,channel], percentageThreshold)
        return ret
    
    else:
        raise ValueError("Wrong dimension: ".format(dim))
