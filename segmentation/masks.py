import numpy as np

def circle(diameter, value=255, dtype=np.uint8):
    """ A circle with set diameter

    Parameters
    ----------
    diameter : np.int
        Sets the width/heigth of the array, which is filled with a circle with according diameter
    value
        The value with wich the circle gets filled
    dtype : np.dtype
        he dtype of the array

    Returns
    -------
    array_like
        An empty array with ecual width/heigth and a centered circle filled with set value.

    Raises
    ------
    ValueError
        The diameter should at least be positive

    """
    if diameter < 0:
        raise ValueError("diameter too small")

    elif diameter == 0:
        return np.array([], dtype)    
    
    elif diameter == 1:
        return np.array([value], dtype)
    
    elif diameter == 2:
        return np.ones((2,2), dtype) * value
    
    elif diameter == 3:
        return np.array([[0,value,0],
                         [value,value,value],
                         [0,value,0]], dtype=dtype)
    
    elif diameter == 4:
        return np.ones((4,4), dtype) * value
        
    else:
        circle = np.zeros((diameter,diameter))

        mid = (np.array(circle.shape)-1) / 2
        for y in range(circle.shape[0]):
            for x in range(circle.shape[1]):
                if (x-mid[1])**2+(y-mid[0])**2 < (diameter/2)**2:
                    circle[y,x] = 1

        return np.asarray(circle, dtype)*value